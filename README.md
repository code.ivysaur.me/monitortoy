# monitortoy

![](https://img.shields.io/badge/written%20in-Javascript%20%28canvas%29-blue)

A utility to help measure and layout a multimonitor setup.

Features: Add monitors from a given diagonal and aspect ratio, swivel monitors, snap to edges, and measure between any two vertices.


## Download

- [⬇️ monitortoy.htm](dist-archive/monitortoy.htm) *(7.56 KiB)*
